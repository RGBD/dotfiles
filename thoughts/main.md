# homesick

is a standalone binary

### supported commands:

```
  homesick cd CASTLE              # Open a new shell in the root of the given castle
  homesick clone URI CASTLE_NAME  # Clone +uri+ as a castle with name CASTLE_NAME f...
  homesick commit CASTLE MESSAGE  # Commit the specified castle's changes
  homesick destroy CASTLE         # Delete all symlinks and remove the cloned repos...
  homesick diff CASTLE            # Shows the git diff of uncommitted changes in a ...
  homesick exec CASTLE COMMAND    # Execute a single shell command inside the root ...
  homesick exec_all COMMAND       # Execute a single shell command inside the root ...
  homesick generate PATH          # generate a homesick-ready git repo at PATH
  homesick help [COMMAND]         # Describe available commands or one specific com...
  homesick link CASTLE            # Symlinks all dotfiles from the specified castle
  homesick list                   # List cloned castles
  homesick open CASTLE            # Open your default editor in the root of the giv...
  homesick pull CASTLE            # Update the specified castle
  homesick push CASTLE            # Push the specified castle
  homesick rc CASTLE              # Run the .homesickrc for the specified castle
  homesick show_path CASTLE       # Prints the path of a castle
  homesick status CASTLE          # Shows the git status of a castle
  homesick track FILE CASTLE      # add a file to a castle
  homesick unlink CASTLE          # Unsymlinks all dotfiles from the specified castle
  homesick version                # Display the current version of homesick
```

### organization

- has dir "home", mirroring home structure
- may have ".homesickrc" with executable code

# PLAN

- learn from homesick, homeshick
  + read the source code
- analyze their conventions, assumptions
- decide, what do i need from my dotfile manager
- stub functionality
- code
- ...
- PROFIT!

### DO IT!

- learn

- analyze

### decide
- require as few actions as possible minimum commands to deploy
- idea of packs. having mutliple repos is important
- having a way track required executables and install them via external pacman
- link a file
- unlink a file
- no other person's repos. if i need functionality, i adapt it for my needs

- stub

- code
