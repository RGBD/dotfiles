export PATH="$PATH:$HOME/.bin:$HOME/.bin/specific"

# # chromium in RAM
# rm -rf "$HOME/.config/chromium"
# rm -rf "/tmp/$USER/chromium"
# mkdir -p "/tmp/$USER/chromium"
# ln -s    "/tmp/$USER/chromium" "$HOME/.config/chromium"
# mkdir -p "$HOME/.config/chromium/cache"
# ln -s    "$HOME/.config/chromium/cache" "$HOME/.cache/chromium"

# firefox hardcore cache to ram
#mkdir -p "/tmp/$USER/firefox-cache"
#mkdir -p "$HOME/.cache/mozilla"

# fix gnome ignoring xorswitch to defaulting to wayland sessions shouldn't have hswitch to defaulting to wayland sessions shouldn't have hg touchpad settings
#configure-touchpad

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export PATH="$PATH:$HOME/.cargo/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export PATH=$PATH:$HOME/.npm-global/bin/
. "$HOME/.cargo/env"
