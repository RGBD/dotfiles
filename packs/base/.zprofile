export EDITOR='nvim'
export VISUAL='nvim'
export PAGER='less'

TERMINAL_EMULATOR=gnome-terminal
TERMINAL_EDITOR='gvim -v'
export PYTHONDONTWRITEBYTECODE=1

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

# [[ -z "$LANG" ]] && export LANG='en_US.UTF-8'

PATH="$PATH:$HOME/.local/bin"
PATH="$PATH:$HOME/.npm-global/bin"
PATH="$PATH:$HOME/.bin"
export PATH

# Ensure path arrays do not contain duplicates.
typeset -gU cdpath fpath mailpath path

# less default settings
export LESS='-FgiMRSwXz-4'

# Create TMPDIR
if [[ ! -d "$TMPDIR" ]]; then
  export TMPDIR="/tmp/$LOGNAME"
  mkdir -p -m 700 "$TMPDIR"
fi

TMPPREFIX="${TMPDIR%/}/zsh"

export AURDEST="${TMPDIR}/aur-cache"
