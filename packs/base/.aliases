# Bash compatible shell aliases and functions

## Remove unwanted aliases
unalias sl 2>/dev/null || true
unalias sudo 2>/dev/null || true

## Vim-like behaviour
alias :e='$EDITOR'
alias :q='exit'
alias :so='source'

## Allow alias expansion
alias watch='watch '

## Add default options
alias node='node --harmony'
alias tree='tree -I "node_modules|.git|tmp/cache" --dirsfirst'
alias rg='rg -i'
alias ag='rg'
alias du='du -h --max-depth=2'
alias gzip='gzip -k'
alias gunzip='gunzip -k'
alias rsync='rsync -rtv --progress --partial'
alias vi='$EDITOR'

## True aliases
alias py2='python2'
alias py3='python3'
alias trash='gvfs-trash'
alias def='sdcv'
alias tnr='tmuxinator'
[[ -x "$(which gvim)" ]] && alias vim='gvim -v'
[[ -x "$(which nvim)" ]] && alias vim='nvim'
alias ta='tmux attach -t'
alias be='bundle exec'
alias beh='bundle exec hanami'

## Made up
alias igrep='grep -i'
alias glog='git log --all --graph'
alias hlog='git hlog'
alias clog='git clog'
alias plog='git plog'
alias gdc='git diff --cached'
# alias du0='du --max-depth=0'
# alias du1='du -h --max-depth=1 | sort -rh'
du1() { du -h --max-depth=1 "$@" | sort -h; }
# alias wanip='dig +short myip.opendns.com @resolver1.opendns.com'
alias wanip='curl https://ipinfo.io/ip'
alias gr='cd $(git rev-parse --show-toplevel 2>/dev/null || pwd)'
echo2() { echo "$@" 2>&2; }
hl() { term=$1; shift 1; grep -i --color -E "$term|$" "$@"; }
fn() { ls -d -- **/*"$1"*; }
mkcd() { command mkdir -p -- "$1" && cd "$1" || return; }
alias C='cls; '
alias serve='python3 -m http.server'
alias showpath='echo $PATH | tr ":" "\n"'
alias pingaround='nmap -sP 192.168.100.0-32 -oG -'
alias diary='vim ~/docs/diary.md.gpg'

# ruby run gem not in a gemfile
rgr() { gem="$1"; shift; ruby "$(which "$gem")" "$@"; }

# pipe system clipboard through filter
pipe() { xsel -b | "$@" | xsel -ib; }

## Workarounds
alias tb='tmux show-buffer | xsel -ib' # copy tmux buffer to clipboard
alias man='LANG=C man' # cannot grep '-' in some manpages with utf-8
alias cls='printf "\ec"; [[ ! -z $TMUX ]] && tmux clear-history'

# global rename
global_rename() { xargs rg "$1" -l | xargs -n1 sed -i "s/$1/$2/g"; }
alias ggrename='git ls-files | global_rename '

## Machine specific, not meant to be shared.
[[ -s ~/.aliases.specific ]] && source ~/.aliases.specific

# vim: ft=sh
