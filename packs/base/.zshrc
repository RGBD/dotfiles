# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

setopt nullglob
unsetopt extendedglob
unsetopt nomatch

## Exports
#export TERM=xterm-256color
export PYTHONDONTWRITEBYTECODE=1
# export PYTHONSTARTUP="$HOME/.python2-startup"
export HISTSIZE=32000
export SAVEHIST=32000
export FILE_MANAGER='nautilus'
export DEFAULT_USER='oleg'
GPG_TTY=$(tty)
export GPG_TTY
export NLTK_DATA="$HOME/.nltk_data"
# export FZF_DEFAULT_COMMAND='rg --files'
export FZF_DEFAULT_COMMAND='rg --files --hidden --follow -g "!{.git,node_modules,tmp,log,dist}/*" 2> /dev/null'
export MOZ_USE_XINPUT2=1
export NVIM_TUI_ENABLE_TRUE_COLOR=1
export ANDROID_EMULATOR_USE_SYSTEM_LIBS=1
export MOZ_ALLOW_GTK_DARK_THEME=true
export NODE_MAX_INSTANCES=5
export NODE_MAX_SESSION=5

EDITOR='vim'
[[ -x "$(which gvim)" ]] && EDITOR='gvim -v'
[[ -x "$(which nvim)" ]] && EDITOR='nvim'
export {VISUAL,EDITOR}=$EDITOR

export PYTHONDONTWRITEBYTECODE=1

# [[ -z "$LANG" ]] && export LANG='en_US.UTF-8'
# export LC_TIME='en_GB.UTF-8'

PATH="$PATH:$HOME/.local/bin"
PATH="$HOME/.npm-global/bin:$PATH"
PATH="$PATH:$HOME/.bin"
export PATH

# less default settings
export LESS='-FgiMRSwXz-4'

case "$HOST" in
  dell|flex) prompt 'agnoster' ;;
  *) prompt 'sorin' ;;
esac

for f in ~/.reminders/*.sh; do
  source "$f"
done

[[ -s "$HOME/.zshrc.local" ]] && source "$HOME/.zshrc.local"

# Aliases

## use gnu time
disable -r time

## source aliases that work in bash and zsh
[[ -s "$HOME/.aliases" ]] && source "$HOME/.aliases"

## Zsh global aliases
alias -g CL='| wc -l'
alias -g H='| head'
alias -g L="| less"
alias -g N="> /dev/null"
alias -g S='| sort'
alias -g G='| grep'

alias rm='nocorrect rm'

## Zsh specific functions
alias rake='noglob rake '

# https://www.commandlinefu.com/commands/view/6148
bindkey '^U' backward-kill-line
bindkey '^Y' yank

### Open dir in file manager
f() {
  file_path=$([[ -z $1 ]] && pwd || echo "$@")
  echo "$file_path"
  $FILE_MANAGER "$file_path" >/dev/null 2>&1 & disown
}
alias fm f

## Zsh callbacks

## ls on cd

chpwd() {
  emulate -L zsh
  ls -a
}

### Display last command execution time

# preexec() {
#   timer=${timer:-$SECONDS}
# }

# precmd() {
#   if [[ ! -z "$timer" ]]; then
#     timer_show=$(( SECONDS - timer ))
#     if [[ timer_show -gt 2 ]]; then
#       export RPROMPT="%F{cyan}${timer_show}s"
#     else
#       export RPROMPT=""
#     fi
#     unset timer
#   fi
# }

source_if_exists() {
  for f in "$@"; do
    [[ -s "$f" ]] && source $f
  done
}

load_nvm() {
  source /usr/share/nvm/init-nvm.sh
}

export AURDEST="${TMPDIR}/aur-cache"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

export PATH="/home/oleg/.rvm/gems/ruby-2.6.3/bin:$PATH"
export PATH="/home/oleg/.rvm/rubies/ruby-2.6.3/bin:$PATH"
export PATH="/home/oleg/.nvm/versions/node/v12.6.0/bin:$PATH"
source /usr/share/nvm/init-nvm.sh
