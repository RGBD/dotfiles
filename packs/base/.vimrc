set nocompatible
filetype off

call plug#begin('~/.vim/bundle')
"Plug 'gmarik/Vundle.vim'

"# Plugins

"## Appearance
"Plug 'altercation/vim-colors-solarized'
"Plug 'frankier/neovim-colors-solarized-truecolor-only'
Plug 'lifepillar/vim-solarized8'
"Plug 'vim-scripts/google'
"Plug 'flazz/vim-colorschemes'
"Plug 'file:///home/oleg/projects/vim/railscasts-colorscheme'
"Plug 'jpo/vim-railscasts-theme'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
"Plug 'terryma/vim-smooth-scroll'
"Plug 'junegunn/seoul256.vim'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'junegunn/limelight.vim'
Plug 'ap/vim-css-color'

"## Completion and search
"Plug 'rking/ag.vim'
Plug 'jiangmiao/auto-pairs'
"Plug 'Raimondi/delimitMate'
" Plug 'Townk/vim-autoclose'
" Plug 'kien/ctrlp.vim'
Plug 'tpope/vim-endwise'
"      \'do': './install.py --clang-completer --rust-completer --system-libclang',
Plug 'Valloric/YouCompleteMe', {
      \'do': './install.py --rust-completer --ts-completer --clang-completer',
      \}
      "'for': ['c', 'cpp', 'ruby', 'py']
"Plug 'vim-syntastic/syntastic', { 'for': ['sh', 'zsh', 'javascript', 'ruby'] }
Plug 'neomake/neomake'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

"## Keymaps and improvements
Plug 'junegunn/vim-easy-align'
"Plug 'Lokaltog/vim-easymotion'
"Plug 'scrooloose/nerdcommenter'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'mbbill/undotree'
Plug 'tpope/vim-commentary'
Plug 'danro/rename.vim'
Plug 'jremmen/vim-ripgrep'

"## Integration
Plug 'tpope/vim-fugitive'
Plug 'christoomey/vim-tmux-navigator'
Plug 'lyokha/vim-xkbswitch'
Plug 'jamessan/vim-gnupg'
"Plug 'edkolev/tmuxline.vim'

"## Interface
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'chrisbra/vim-diff-enhanced'
Plug 'majutsushi/tagbar'
Plug 'scrooloose/nerdtree', { 'on': ['NERDTree', 'NERDTreeToggle'] }
"Plug 'junegunn/goyo.vim'
Plug 'jceb/vim-orgmode'

"## Language support
Plug 'mxw/vim-jsx'
Plug 'RGBD/vim-fix-sw-indent'
"Plug 'sheerun/vim-polyglot'
Plug 'kchmck/vim-coffee-script', { 'for': ['coffee'] }
Plug 'mattn/emmet-vim', { 'for': ['html', 'less', 'slim', 'eruby'] }
"Plug 'git://git.code.sf.net/p/vim-latex/vim-latex'
"Plug 'lervag/vimtex'
Plug 'groenewege/vim-less', { 'for': ['less'] }
"Plug 'plasticboy/vim-markdown', { 'for': ['markdown'] }
" Plug 'tpope/vim-markdown', { 'for': ['markdown'] }
" Plug 'gabrielelana/vim-markdown', { 'for': ['markdown'] }
Plug 'hynek/vim-python-pep8-indent', { 'for': ['python'] }
Plug 'vim-ruby/vim-ruby', { 'for': ['ruby', 'eruby'] }
Plug 't9md/vim-ruby-xmpfilter', { 'for': ['ruby'] }
"Plug 'vim-scripts/google.vim'
Plug 'vim-scripts/ruby-matchit', { 'for': ['ruby'] }
Plug 'slim-template/vim-slim', { 'for': ['slim'] }
Plug 'vim-scripts/svg.vim', { 'for': ['svg'] }
Plug 'kongo2002/fsharp-vim', { 'for': ['fsharp'] }
Plug 'pangloss/vim-javascript', { 'for': ['javascript'] }
Plug 'leafgarland/typescript-vim' , { 'for': ['typescript'] }
Plug 'tpope/vim-rails' " , { 'for': ['ruby', 'eruby', 'yaml'] }
Plug 'tpope/vim-haml', { 'for': ['scss'] }
" Plug 'maksimr/vim-jsbeautify' ", { 'for': ['html', 'javascript'] }
Plug 'rust-lang/rust.vim'
Plug 'vim-scripts/nginx.vim'
Plug 'rgbd/vim-xml-indent'
Plug 'dsawardekar/portkey'
Plug 'dsawardekar/ember.vim'
Plug 'joukevandermaas/vim-ember-hbs'

"## Various fixes and patches
"Plug 'file:///home/oleg/projects/vim/xml-indent'
"Plug 'takac/vim-hardtime'
"Plug 'MarcWeber/vim-addon-local-vimrc'
Plug 'vim-scripts/bufkill.vim'
Plug 'tpope/vim-repeat'
"Plug 'file://' . expand('$HOME/projects/install/web-indent')
call plug#end()

filetype plugin indent on

"# Settings

"## Appearance
if has('termguicolors')
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  set termguicolors
endif
set background=dark
set colorcolumn=+1,+21,+41
"set cursorcolumn
"set cursorline
set gcr=a:blinkon0
set history=1000
set laststatus=2
set lazyredraw
set number
set showcmd
set showmode
syntax on
set listchars=trail:·,tab:->
set list
let g:netrw_banner = 0

"### Bell
set noerrorbells
set t_vb= " no blinking
set visualbell

"### Timeout
set timeoutlen=1000 ttimeoutlen=0

"### Gui settings
set guioptions-=m guioptions-=T
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline"\ 11
set mouse=a
set mousemodel=popup_setpos

"## Completion
set wildmenu
set wildmode=longest:full,full " rich command suggestion
set wildignorecase
set wildignore=*.o,*.obj,*~
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif
set wildignore+=target/**

"## Folding
set foldmethod=indent
set nofoldenable

"## Indent
set tabstop=2
set shiftwidth=2
set softtabstop=-1
set expandtab
" set cinoptions=g0,N-s,+2s,(0,W2s,j1,J1,:0
set cinoptions=g0,N-s,+2s,W2s,j1,J1,:0
let g:html_indent_script1 = "inc"
let g:html_indent_style1 = "inc"
let g:html_indent_inctags = "p,option"

"## Scrolling
set scrolloff=3
set sidescrolloff=1
set sidescroll=0

"## Using external files
set secure " don't allow changing files

"set autoread
set backspace=indent,eol,start
set hidden
set autoindent
set smartindent
"set linebreak
set nowritebackup

"## Search
set ignorecase
set smartcase
set incsearch
set hlsearch

"## Wrapping
set textwidth=80
set nojoinspaces

set formatoptions-=t
autocmd! BufNewFile,BufRead *.txt,*.md setlocal formatoptions+=t

"## Clipboard
"set clipboard=unnamedplus

"## Do not move files, breaks webpack watcher
" set backupcopy=yes

"# Keybindings
"## Platform specific keyboard bindings
if filereadable(expand("~/.vim-kbd"))
  source ~/.vim-kbd
endif

autocmd FileType help,vim nnoremap K :help<Space><C-r><C-w><CR>
noremap ^ 0
noremap 0 ^

"## Auto indent pasted text

nnoremap gp p=`]<C-o>
nnoremap gP P=`]<C-o>

" nnoremap p p=`]<C-o>
" nnoremap P P=`]<C-o>

" nnoremap gp p
" nnoremap gP P

"## Plain text editor key bindings.
inoremap <silent><S-Tab> <C-o><<
"nnoremap <Leader><CR> :set nohlsearch<CR>
nnoremap <Leader>v :set paste<Cr>i<C-r>+<Esc>:set nopaste<Cr>
vnoremap <C-c> "+y

" TODO: make it function
let hlstate=0
nnoremap <silent> <Leader><CR> :if (hlstate%2 == 0) \| nohlsearch \| else \| set hlsearch \| endif \| let hlstate=hlstate+1<cr>

"## Moving lines holding Alt
" nnoremap <silent><A-j> :m .+1<CR>==
" nnoremap <silent><A-k> :m .-2<CR>==
" inoremap <silent><A-j> <Esc>:m .+1<CR>==gi
" inoremap <silent><A-k> <Esc>:m .-2<CR>==gi
" vnoremap <silent><A-j> :m '>+1<CR>gv=gv
" vnoremap <silent><A-k> :m '<-2<CR>gv=gv

"## I MUST learn VIM. The hardest way.
:noremap <up> <nop>
:noremap <down> <nop>
:noremap <left> <<
:noremap <right> >>

:inoremap <up> <nop>
:inoremap <down> <nop>
:inoremap <left> <nop>
:inoremap <right> <nop>

"# Plugin Settings

"## vim-ripgrep
command! Ag Rg

"## vim-airline
let g:airline_powerline_fonts = 1 " ($DISPLAY != '')
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = 'solarized'

"## solarized
"let g:solarized_contrast = "high"
silent! colorscheme solarized8

"## hardtime
let g:hardtime_showmsg=1
let g:hardtime_maxcount=5
"autocmd BufEnter * HardTimeOn

"## vim-indent-guides
autocmd VimEnter * call CheckedIndentGuidesEnable()
function! CheckedIndentGuidesEnable()
  if exists(":IndentGuidesEnable")
    execute "IndentGuidesEnable"
  endif
endfunction

"## xkb-switch
let g:XkbSwitchLib = '/usr/local/lib/libg3kbswitch.so'
let g:XkbSwitchEnabled=1

"## YouCompleteMe settings
let g:ycm_error_symbol=">>"
let g:ycm_warning_symbol="!!"
let g:ycm_allow_changing_update_time=1
let g:ycm_autoclose_preview_window_after_insertion=1
let g:ycm_confirm_extra_conf=0
let g:ycm_global_ycm_extra_conf='~/.ycm_extra_conf.py'
let g:ycm_collect_identifiers_from_tags_files=1
let g:ycm_server_python_interpreter='/usr/bin/python3'
" let g:ycm_rust_src_path = expand("$HOME/.local/share/rust/src")
autocmd FileType c nnoremap <buffer> <silent> <C-]> :YcmCompleter GoTo<cr>

"## Syntastic
" let g:syntastic_check_on_open = 0
" let g:syntastic_check_on_wq = 0
" let g:syntastic_check = 0
" let g:syntastic_mode_map = { 'mode': 'active', 'active_filetypes': [],
"       \'passive_filetypes': [] }
" noremap <C-w>e :SyntasticCheck<CR>
" noremap <C-w>f :SyntasticCheck<CR>:SyntasticToggleMode<CR>

" let g:syntastic_disabled_filetypes=['sass', 'slim']
" let g:syntastic_sh_shellcheck_args = "-e SC1090 -e SC2148"
" let g:syntastic_javascript_checkers=['eslint']
" let g:syntastic_ruby_checkers=['rubocop']

"## EasyMotion
" map <Leader> <Plug>(easymotion-prefix)

"## NERDTree
let g:nerdtree_tabs_autoclose=1

"## EasyAlign
vmap <CR> <Plug>(EasyAlign)

"## Gitgutter
let g:gitgutter_max_signs=1000
nnoremap [h :GitGutterPrevHunk<Cr>
nnoremap ]h :GitGutterNextHunk<Cr>
nnoremap <Leader>guh :GitGutterUndoHunk<Cr>

"## CtrlP
"if (executable('ag'))
  "let g:ctrlp_user_command = 'ag %s -i --nocolor --nogroup --hidden
        "\ --ignore .git
        "\ --ignore .svn
        "\ --ignore .hg
        "\ --ignore .DS_Store
        "\ --ignore "**/*.pyc"
        "\ -g ""'
"endif

"## smooth-scroll
" nnoremap <silent> <C-u> :call smooth_scroll#up  (&scroll  , 15, 4)<CR>
" nnoremap <silent> <C-d> :call smooth_scroll#down(&scroll  , 15, 4)<CR>
" nnoremap <silent> <C-b> :call smooth_scroll#up  (&scroll*2, 15, 8)<CR>
" nnoremap <silent> <C-f> :call smooth_scroll#down(&scroll*2, 15, 8)<CR>

"## ruby-xmpfilter

function! XmpClear()
  %s/\s*# \(=>\|>>\|\~>\|!>\).*$//
  call RemoveBlankEndLines()
endfunction
command! XmpClear call XmpClear()

autocmd FileType ruby nmap <buffer> <A-m> <Plug>(xmpfilter-mark)
autocmd FileType ruby xmap <buffer> <A-m> <Plug>(xmpfilter-mark)
autocmd FileType ruby imap <buffer> <A-m> <Plug>(xmpfilter-mark)

autocmd FileType ruby nmap <buffer> <A-r> <Plug>(xmpfilter-run)
autocmd FileType ruby xmap <buffer> <A-r> <Plug>(xmpfilter-run)
autocmd FileType ruby imap <buffer> <A-r> <Plug>(xmpfilter-run)

autocmd FileType ruby nmap <buffer> <A-c> :call XmpClear()<CR>
autocmd FileType ruby xmap <buffer> <A-c> :call XmpClear()<CR>
autocmd FileType ruby imap <buffer> <A-c> :call XmpClear()<CR>

let g:xmpfilter_cmd = "seeing_is_believing"

"## typescript-vim
"let g:typescript_indent_disable=1

"## vim-ruby
"let g:ruby_indent_access_modifier_style="indent"

"## limelight.vim
let g:limelight_conceal_ctermfg = 'blue'
let g:limelight_conceal_guifg = '#002836'

"## rainbow_parentheses
let g:rainbow#pairs = [['(', ')'], ['[', ']'], ['{', '}']]
autocmd VimEnter * call RainbowParenthesesEnable()
function! RainbowParenthesesEnable()
  if exists(":RainbowParentheses")
    execute "RainbowParentheses"
  endif
endfunction

"## fzf
nnoremap <C-p> :Files<Cr>

"## rainbow-parentheses
" fragile
let g:rainbow#blacklist = ['#586e75', '#657b83']

"## numbertoggle
let g:NumberToggleTrigger="<Leader>n"

"## neomake
call neomake#configure#automake('w')

"# Extra commands

autocmd FileType slim set formatoptions-=tc

nnoremap <Leader>b :Buffers<Cr>

"## Remove Trailing Spaces
command! NoTrailSpaces call RemoveTrailingWhitespaces()
function! RemoveTrailingWhitespaces()
  let cursor_keep = getpos('.')
  %s/\s\+$//e
  call setpos('.', cursor_keep)
endfun

"## Remove blank ending lines
command! NoBlankEndLines call RemoveBlankEndLines()
function! RemoveBlankEndLines()
  let cursor_keep = getpos('.')
  %s#\($\n\s*\)\+\%$##e
  call setpos('.', cursor_keep)
endfunction

"## Remove all unwanted artifacts
command! NoUnwantedArtifacts call NoUnwantedArtifacts()
function! NoUnwantedArtifacts()
  call RemoveTrailingWhitespaces()
  call RemoveBlankEndLines()
endfunction

"## Save buffer on exit
autocmd VimLeave * call system("xsel -ib", getreg('+'))

"## Insert date and time
inoremap <F4> <C-r>=split(system('date -I'), "\n")[0]<CR>
inoremap <F5> <C-r>=split(system('date -Is'), "\n")[0]<CR>

command! -bang Sh silent call Shebang(<bang>0)

function! SetExecutableBit()
  " This function is taken from
  " http://vim.wikia.com/wiki/Setting_file_attributes_without_reloading_a_buffer
  " Thanks Max Ischenko!
  let fname = expand("%:p")
  checktime
  execute "au FileChangedShell " . fname . " :echo"
  silent write
  silent !chmod a+x %
  checktime
  execute "au! FileChangedShell " . fname
endfunction

function! Shebang(bang)
  call SetExecutableBit()
  let cursor_keep = getpos('.')
  let SHEBANGS =
        \ {
        \   'javascript' : '#!/usr/bin/env node',
        \   'python'     : '#!/usr/bin/env python3',
        \   'ruby'       : '#!/usr/bin/env ruby',
        \   'sh'         : '#!/bin/bash',
        \ }
  let shift=0
  if has_key(SHEBANGS, &filetype)
    if getline(1) !~ '^#!'
      call append(0, [SHEBANGS[&filetype], ''])
      let shift = shift + 2
    elseif a:bang != 0
      1 delete
      let shift = shift - 1
      if getline(1) =~ '^\s*$'
        1 delete
        let shift = shift - 1
      endif
      call append(0, [SHEBANGS[&filetype], ''])
      let shift = shift + 2
    endif
  endif
  let cursor_keep[1] = cursor_keep[1] + shift
  echom shift
  call setpos('.', cursor_keep)
endfunction

"## Execute current or selected linees
au BufRead ~/.vimrc nnoremap <C-x>
      \:exe getline(".")<CR>
au BufRead ~/.vimrc vnoremap <C-x>
      \ :<C-w>exe join(getline("'<", "'>"), "\n")<CR>

"## Open toc for configs, close on enter
au BufRead  ~/.*rc,~/.tmux.conf,~/.aliases nnoremap <buffer> <C-i>
      \ :lvimgrep /^.\?#\+ \w.*$/ % \| lopen <CR>
au FileType qf nnoremap <buffer> <CR> <CR>:lclose<CR>

"## Vim save session and quit
function! SaveSessionAndQuit()
  wall
  mksession! ~/tmp/mks
  quit
endfunction

command! Q :call SaveSessionAndQuit()

"### binding.pry
au FileType ruby nnoremap <Leader>p Obinding.pry<Esc>==j0

colorscheme solarized8
