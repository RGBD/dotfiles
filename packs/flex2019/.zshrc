[[ -s "$HOME/.config/zplug/zplugrc" ]] && source "$HOME/.config/zplug/zplugrc"

# Settings
SAVEHIST=100000
HISTSIZE=100000
HISTFILE=~/.zsh_history

PROMPT='%F{green}%n%f@%F{magenta}%m%f %F{blue}%B%~%b%f %# '

# Aliases

# abbreviations
alias e="$EDITOR"
alias vim="$EDITOR"
alias ag='rg'
alias o='xdg-open'

# default options
alias rsync='rsync -rtv --partial --progress'
alias ls='ls --group-directories-first'

# custom commands
alias cls='printf "\ec"'
alias du1='du -h --max-depth=1 | sort -h'
f() { nautilus "${1-.}" & disown $1; }

# Add RVM to PATH for scripting.
export PATH="$PATH:$HOME/.rvm/bin"
