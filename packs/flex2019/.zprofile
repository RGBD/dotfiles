export EDITOR='nvim'
export LC_COLLATE=C
export PYTHONDONTWRITEBYTECODE=1
export PAGER=less
export MOZ_USE_XINPUT2=1

export PATH="$PATH:$HOME/.bin"
export PATH="$PATH:$HOME/.local/share/npm/bin"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
