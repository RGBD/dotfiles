call plug#begin('~/.vim/bundle')
" Appearance
Plug 'morhetz/gruvbox'
" Language support
Plug 'jamessan/vim-gnupg'
Plug 'ycm-core/YouCompleteMe', { 'do': './install.py --rust-completer' }
Plug 'tpope/vim-commentary'
" Extra features
Plug 'scrooloose/nerdtree'
Plug 'itchyny/lightline.vim'
Plug 'mengelbrecht/lightline-bufferline'
call plug#end()

" Appearance

set colorcolumn=81,101,121
set termguicolors
set showtabline=2

let g:gruvbox_contrast_dark = 'hard'
colorscheme gruvbox

" Behaviour

set hidden
set textwidth=80
" set ts=2 sw=0 sts=-1 et
set et
set ignorecase smartcase
set incsearch
set hlsearch
set number
set mouse=a

" Override ftplugin

autocmd Filetype markdown setlocal formatoptions-=l

"# Plugin settings

"# Lightline
let g:lightline                  = {}
let g:lightline.tabline          = { 'left': [['buffers']], 'right': [] }
let g:lightline.component_expand = { 'buffers': 'lightline#bufferline#buffers' }
let g:lightline.component_type   = { 'buffers': 'tabsel' }
let g:lightline.component_raw = {'buffers': 1}
let g:lightline#bufferline#clickable = 1

"# Custom commands

"## Insert date and time

inoremap <F4> <C-r>=split(system('date -I'), "\n")[0]<CR>
inoremap <F5> <C-r>=split(system('date -Is'), "\n")[0]<CR>
