#!/bin/bash

cd "$(dirname "$0")" || {
  echo "couldnt cd to script dir"
  exit 1
}

mkdir -p ~/.opt/git-sync
# rm -rf ~/.opt/git-sync
git clone 'https://github.com/simonthum/git-sync' ~/.opt/git-sync
mkdir -p ~/.config/systemd/user
cp git-sync-docs.service git-sync-docs.timer ~/.config/systemd/user/

systemctl --user start git-sync-docs
systemctl --user enable git-sync-docs.timer
systemctl --user start git-sync-docs.timer
