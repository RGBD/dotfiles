#!/bin/bash

[[ $EUID -eq 0 ]] || {
  echo "must be root"
  exit 1
}
cd "$(dirname "$0")" || {
  echo "couldnt cd to script dir"
  exit 1
}

cp lenovo-keymap-custom lenovo-keymap-default /usr/bin/
cp lenovo-keymap-custom.service /etc/systemd/system/

systemctl enable lenovo-keymap-custom
systemctl start lenovo-keymap-custom
