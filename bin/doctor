#!/bin/bash

RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
BOLD="$(tput bold)"
RESET="$(tput sgr0)"

echo "$RED $GREEN $BOLD $RESET" >/dev/null

echo2() { echo "$@" 1>&2; }

usage() {
  echo2 "usage: $0 group"
  echo2 "checks if executables in group are present"
}

check() {
  if [[ $# -ne 1 ]]; then
    echo2 "usage: $0 executable"
    echo2 "check if executable is in path."
    return
  fi
  executable_path="$(which "$1" 2>/dev/null)"
  name="$(printf "%-20s" "$1")"
  if [[ -x "$executable_path" ]]; then
    if [[ ! -z "$ONLY_FAILS" ]]; then
      return
    fi
    echo -e "$name: ${GREEN}OK${RESET} ($executable_path)"
  else
    echo -e "$name: ${RED}FAIL${RESET}"
  fi
}

SELF_PATH="$(realpath "$0")"
GROUPS_PATH="$(dirname "$SELF_PATH")/groups"

if [[ ( $# -eq 1 ) && ( $1 == "fails" ) ]]; then
  ONLY_FAILS='yes'
fi

for g in $GROUPS_PATH/*; do
  group_name=$(basename "$g")
  echo "GROUP: $group_name"
  while read -r executable; do
    check "$executable"
  done < "$g"
  echo
done
