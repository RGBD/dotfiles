#!/bin/bash

DOTFILES_ROOT="$HOME/.dotfiles/packs/base"

dry_run() {
  echo "$@"
  [[ ! -z "$DEPLOY_NO_DRY_RUN" ]] && "$@"
}

echo2() {
  echo "$@" >&2
}

backup_file() {
  if [[ $# -ne 1 ]]; then
    echo2 "usage: backup_file <filename>"
    return
  fi
  if [[ ! -f $1 ]]; then
    echo2 "backup_file: file not found: $1"
    return
  fi
  if [[ ! -z "$DEPLOY_NO_BACKUPS" ]]; then
    echo2 "skipping backup: $1"
    return;
  fi
  local backup_name_prefix="$1.bak"
  local backup_version=0
  while true; do
    local backup_name="$backup_name_prefix.$backup_version"
    if [[ ! -f "$backup_name" ]]; then
      dry_run cp "$1" "$backup_name"
      break
    fi
    backup_version=$((backup_version + 1))
  done
}

link_all() {
  find "$DOTFILES_ROOT" -type f -print0 |
  while IFS= read -r -d $'\0' f_with_prefix; do
    f=${f_with_prefix#$DOTFILES_ROOT/}
    if [[ -f "$HOME/$f" ]]; then
      backup_file "$HOME/$f"
    fi
    dry_run mkdir -p "$(dirname "$HOME/$f")"
    dry_run ln -sf --relative --\
      "$(realpath "$DOTFILES_ROOT/$f")" "$(realpath -s "$HOME/$f")"
  done
}

[[ "${BASH_SOURCE[0]}" != "${0}" ]] && return

DEPLOY_NO_BACKUPS=yes DEPLOY_NO_DRY_RUN=yes link_all
